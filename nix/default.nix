{ pkgs, stdenv, opam2nix, fetchFromGitHub, makeRustPlatform }:

let
  src = fetchFromGitHub {
    owner = "mozilla";
    repo = "nixpkgs-mozilla";
    rev = "bf862af36145f5c1e566f93fe099e36bddeac0c8";
    sha256 = "0g6a0jss350k1ki7srhwkx62x93gxi2zxjdlii0m4bn5i7pxfcn4";
  };
in
with import "${src.out}/rust-overlay.nix" pkgs pkgs;

let
  rustPlatform = makeRustPlatform {
    cargo = latest.rustChannels.stable.rust;
    rustc = latest.rustChannels.stable.rust;
  };

  blake2b_simd_rs = rustPlatform.buildRustPackage rec {
    name = "blake2b_simd_rs-${version}";
    version = "0.1";
    src = ../.;
    cargoSha256 = "11zhsjwdm6aqzbykxksvqlb8c418z2pspw2ci46gr54r2awr1bak";
    meta = with stdenv.lib; {
      description = "A stub for blake2b_simd.";
      homepage = https://gitlab.com/et4te/ocaml-blake2b-simd;
      license = licenses.mit;
    };
  };
in

with stdenv.lib;

stdenv.mkDerivation {
    name = "blake2b-simd";
    src = ../.;
    buildInputs = [blake2b_simd_rs] ++ opam2nix.build {
      specs = opam2nix.toSpecs [ "dune" "ocamlbuild" "ocamlfind" ];
      ocamlAttr = "ocaml";
      ocamlVersion = "4.06.1";
    };
    buildPhase = ''
      mkdir -p target/release
      cp ${blake2b_simd_rs}/lib/libblake2b_simd_stubs.a target/release/libblake2b_simd_stubs.a
      cp ${blake2b_simd_rs}/lib/libblake2b_simd_stubs.so target/release/libblake2b_simd_stubs.so
      dune build
    '';
    installPhase = ''
      dune runtest
      cp -R _build $out/lib/blake2b_simd
    '';
}

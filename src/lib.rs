#[macro_use]
extern crate ocaml;
extern crate blake2b_simd;

use ocaml::ToValue;
use blake2b_simd::Params;

caml!(hash_bytes, |message_handle,key_handle,length_handle|, <dest>, {
    let message: Vec<u8> = ocaml::FromValue::from_value(message_handle);
    let key: String = ocaml::FromValue::from_value(key_handle);
    let length: i32 = ocaml::FromValue::from_value(length_handle);
    let hash = Params::new()
        .hash_length(length as usize)
        .key(key.as_bytes())
        .to_state()
        .update(&message)
        .finalize();
    let bytes: Vec<u8> = (&hash).as_bytes().to_vec();
    dest = bytes.to_value();
} -> dest);

caml!(hash_string, |message_handle,key_handle,length_handle|, <dest>, {
    let message: String = ocaml::FromValue::from_value(message_handle);
    let key: String = ocaml::FromValue::from_value(key_handle);
    let length: i32 = ocaml::FromValue::from_value(length_handle);
    let hash = Params::new()
        .hash_length(length as usize)
        .key(key.as_bytes())
        .to_state()
        .update(message.as_bytes())
        .finalize();
    let string: String = (&hash).to_hex().to_string();
    dest = string.to_value();
} -> dest);

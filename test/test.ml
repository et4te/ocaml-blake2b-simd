let _ =
  let str = Blake2b_simd.hash_string "a message" "a key" 16 in
  if String.length str <> 32 then
    Printf.printf "Blake2b string length error";
  let arr = Blake2b_simd.hash_bytes (Array.map char_of_int [|0;1;2;3;4;5|]) "a key" 16 in
  if Array.length arr <> 16 then
    Printf.printf "Blake2b hash length error";
  Gc.minor ();
  Gc.full_major ()
